package com.example.projectastronomy.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.VelocityTracker;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.projectastronomy.R;
import com.example.projectastronomy.astro_objects.Universe;
import com.example.projectastronomy.utils.Constants;
import com.example.projectastronomy.utils.Vector2D;

import org.jetbrains.annotations.Nullable;



public class GameView extends SurfaceView implements View.OnClickListener, SurfaceHolder.Callback, Runnable {
    //Crear un hilo
    Thread thread = null;
    volatile boolean isRunning = false;

    // Creamos un objeto del universo que contiene todos los cuerpos celestes
    private Universe universe;
    
    //Dibujar en la pantalla
    Canvas canvas;
    
    // Creamos el surfaceholder
    SurfaceHolder holder;
    //FPS
    long fps;
    private long frameTime;

    // Para  la escala
    private ScaleGestureDetector scaleDetector;
    private float scaleFactor = 0.7f;
    private float focusX;
    private float focusY;
    //para el control de los ejes con respecto a la pantalla
    private float dx = -Constants.UNIVERSEWIDTH / 4;
    private float dy = -Constants.UNVIVERSEHEIGTH  /4;
    private boolean isPanning = false;
    private float panStartX = 0.f;
    private float panStartY = 0.f;
    private float panEndX = 0.f;
    private float panEndY = 0.f;

    //Configuraciones
    //Zoom
    public boolean isZoomMode = false;

    //trace paths
    public boolean isTraceMode = false;

    //Player control mode
    public boolean isplayerShipMode = false;

    // Pintar los limites del universo
    public Paint boundaryPaint = new Paint();
    public Paint joystickPaint = new Paint();
    public Rect boundaryRect = new Rect(0, 0, Constants.UNIVERSEWIDTH, Constants.UNVIVERSEHEIGTH);

    // crear enums
    public RESET_TYPE currentLoadType = RESET_TYPE.SINGLE_STAR_SYSTEM;
    public ADD_TYPE currentAddType = ADD_TYPE.PLANET;
    public PLACEMENT_TYPE currentPlacementType = PLACEMENT_TYPE.SCATTER;
    public SIZE_TYPE currentSizeType = SIZE_TYPE.MEDIUM;

    //Clase de constantes para factor de escala
    public static class SCALEConstants {
        public static float MINSCALE = 0.2f;
        public static float MAXSCALE = 5.0f;
        public static float DEFAULTSCALE = 0.7f;
    }

    //Crear enum para cargar el modo del universo

    public static enum RESET_TYPE {
        BLANK, SINGLE_STAR_SYSTEM, BINARY_STAR_SYSTEM, BLACK_HOLE_ACCELERATION, CIRCLING_SATELLITES;

        // Vamos a convertir los strings d el enum en un string Donde solamente la primera letra este en mayuscuka
        public static String[] getString(){
            String[] strs = new String[RESET_TYPE.values().length];
            int  i = 0;
             for(RESET_TYPE p : RESET_TYPE.values()){
                strs[i++] = p.toString().substring(0,1).toUpperCase()  + p.toString().substring(1).toLowerCase().replace("_"," ");
            }
            return strs;
        }
    }

    //Vamos a crear un enum par a cargar los cuerpos celestes
    public static enum ADD_TYPE{
        ASTEROID, PLANET, STAR,BLACK_HOLE,SATELITE,PLAYER_SHIP,;
        public static String[] getString(){
            String[] strs = new String[ADD_TYPE.values().length];
            int  i = 0;
            for(ADD_TYPE p:  ADD_TYPE.values()) {
                strs[i++] = p.toString().substring(0, 1).toUpperCase() + p.toString().substring(1).toLowerCase().replace("_" ," ");
            }
            return strs;
        }
    }

    //Vamos a crear un enum para carg ar los cuerpos celestes
    public static enum PLACEMENT_TYPE{

        SCATTER, FLICK, TARGET, IDLE, FIXED, ORBIT;
        public static String[] getString(){
            String[] strs = new  String[PLACEMENT_TYPE.values().length];
            int i = 0;
            for(PLACEMENT_TYPE p: PLACEMENT_TYPE.values()){
                 strs[i++] = p.toString().substring(0 ,1).toUpperCase() + p.toString().substring(1).toLowerCase().replace("_ "," ");
            }
            return strs;
        }
    }

    // Crear un enum para el tam año de los cuerpos celestes
    public static enum SIZE_TYPE{
        SMALL, MEDIUM, LARGE, RANDOM;
        public static String getAbbreviation(SIZE_TYPE type) {
            switch (type) {
                case SMALL:
                    return "SM";
                case MEDIUM:
                    return "MD";
                case LARGE:
                    return "LG";
                case RANDOM:
                    return "RND";
            }
            return "";
        }

         public static String[] getString() {
            String[] strs = new String[SIZE_TYPE.values().length];
            int i = 0;
            for (SIZE_TYPE p: SIZE_TYPE.values()) {
                strs[i++] = p.toString().substring(0,1).toUpperCase() + p.toString().substring(1).toLowerCase();
            }
            return strs;
        }

    }
//Metodo constructor para la clase Game View
    public GameView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        holder = getHolder();
        universe = new Universe(RESET_TYPE.BLANK);
        setFocusable(true);
        scaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        boundaryPaint = new Paint();
        boundaryPaint.setStyle(Paint.Style.FILL);
        boundaryPaint.setColor(Color.argb(255,20,20,20));
        joystickPaint = new Paint();
        joystickPaint.setStyle(Paint.Style.FILL);
        joystickPaint.setColor(Color.argb(255,255,255,255));
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
        resume();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        pause();
    }

    public void update() {
        universe.update();
    }

    private VelocityTracker mVelocityTracker = null;

    // Si el usuario mantiene pulsada la pantalla, sigue la posición
    private int xOriginal = 0;
    private int yOriginal = 0;


    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();

        int index = event.getActionIndex();
        int action = event.getActionMasked();
        int pointerId = event.getPointerId(index);

        //Para el zoom
        scaleDetector.onTouchEvent(event);
        String DEBUG_TAG = "Gesture";
        switch(action) {
            // Single tap
            case (MotionEvent.ACTION_DOWN) :
                if(mVelocityTracker == null) {
                    // Retrieve a new VelocityTracker object to watch the
                    // velocity of a motion.
                    mVelocityTracker = VelocityTracker.obtain();
                }else{
                    mVelocityTracker.clear();
                }
                if(!isZoomMode && !isPanning){
                    mVelocityTracker.addMovement(event);

                }
                if (isZoomMode){
                    panStartX = dx + x;
                    panStartY = dy + y;
                    isPanning = true;

                }
                else {
                    universe.addCelestialBody(new Vector2D(Math.round((x-dx)/scaleFactor), Math.round((y-dy)/scaleFactor)),
                            new Vector2D(mVelocityTracker.getXVelocity(pointerId)/scaleFactor,mVelocityTracker.getYVelocity(pointerId)/scaleFactor),
                            action, currentAddType, currentSizeType, currentPlacementType);
                }
                    xOriginal = x;
                    yOriginal = y;

                return true;
            case (MotionEvent.ACTION_MOVE) :
                mVelocityTracker.addMovement(event);
                mVelocityTracker.computeCurrentVelocity(1000);

                if (isPanning && isZoomMode){
                    dx = panStartX - x;
                    dy = panStartY - y;
                    if (-(dx-universe.getScreenWidth())/scaleFactor > (Constants.UNIVERSEWIDTH + Constants.UNIVERSEWIDTH*0.00f)) {
                        dx = -(Constants.UNIVERSEWIDTH + Constants.UNIVERSEWIDTH*0.00f)*scaleFactor + universe.getScreenWidth();
                    }
                    else if (-(dx+universe.getScreenWidth()*0) < 0) {
                        dx = universe.getScreenWidth()*0.0f;
                    }
                    // Check top boundary
                    if (-(dy-universe.getScreenHeight())/scaleFactor > (Constants.UNVIVERSEHEIGTH + Constants.UNVIVERSEHEIGTH*0.00f)) {
                        dy = -(Constants.UNVIVERSEHEIGTH + Constants.UNVIVERSEHEIGTH*0.00f)*scaleFactor + universe.getScreenHeight();
                    }
                    else if (-(dy+universe.getScreenHeight()*0.0f) < 0) {
                        dy = universe.getScreenHeight()*0.0f;
                    }
                }
                else if (universe.isPlayer && currentAddType == ADD_TYPE.PLAYER_SHIP) {
                    universe.setPlayerControls(new Vector2D((x-xOriginal), (y-yOriginal)));
                }
                else{
                    if (!isZoomMode){
                        universe.addCelestialBody(new Vector2D(Math.round(((x-dx)/scaleFactor)), Math.round((y-dy)/scaleFactor)),
                                new Vector2D(mVelocityTracker.getXVelocity(pointerId)/scaleFactor,mVelocityTracker.getYVelocity(pointerId)/scaleFactor),
                                action, currentAddType, currentSizeType, currentPlacementType);
                    }
                }
                return true;

            case (MotionEvent.ACTION_UP) :
                if (!isZoomMode && !isPanning)
                {
                    universe.addCelestialBody(new Vector2D((xOriginal-dx)/scaleFactor, (yOriginal-dy)/scaleFactor),
                            new Vector2D((xOriginal-x)/scaleFactor, (yOriginal-y)/scaleFactor),
                            action, currentAddType, currentSizeType, currentPlacementType);
                }
                // In zoom mode and stopped touching, stop panning
                if (isPanning)
                {
                    panEndX = x;
                    panEndY = y;
                    isPanning = false;

                }
                if (universe.isPlayer) {
                    universe.setPlayerControls(new Vector2D(0,0));
                }

                return true;
            case (MotionEvent.ACTION_CANCEL) :
                mVelocityTracker.recycle();

                return true;
            case (MotionEvent.ACTION_OUTSIDE) :
                return true;

            default :
                return super.onTouchEvent(event);
        }
    }
    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.resetButton:
                break;
        }
    }


     public void reset(GameView.RESET_TYPE requestedPreset, float scale) {
        universe = new Universe(requestedPreset);

        switch (requestedPreset) {
            case CIRCLING_SATELLITES:
                dx = 0;
                dy = 0;
                scaleFactor = SCALEConstants.MINSCALE;
                break;
            default:
                dx = -Constants.UNIVERSEWIDTH/4;
                dy = -Constants.UNVIVERSEHEIGTH/4;
                scaleFactor = SCALEConstants.DEFAULTSCALE;
                break;
        }
        xOriginal = 0;
        yOriginal = 0;
        panStartY = 0;
        panStartX = 0;
        panEndY = 0;
        panEndX = 0;

    }



    public void setCurrentResetType(RESET_TYPE newLoadType){
        currentLoadType = newLoadType;
    }

    public void setCurrentAddType(ADD_TYPE newAddType) {
        currentAddType = newAddType;
    }
    public void setCurrentPlacementType(PLACEMENT_TYPE newPlacementType) {
        currentPlacementType = newPlacementType;
    }


    public void setCurrentSizeType(SIZE_TYPE newSizeType) {
        currentSizeType = newSizeType;
    }
    public void toggleZoomMode() {
        isZoomMode = !isZoomMode;
    }
    public void toggleTraceMode() { isTraceMode = !isTraceMode; }
    public void setCurrentDeltaT(double newDt) {
        universe.setCurrentDt(newDt);
    }
    public void setCurrentSteps(int newSteps) {
        universe.setCurrentDt((double)(1.0/newSteps));
//        Log.d("CURRENTDT", Double.toString(newSteps) + " " + Double.toString((double)(1.0/newSteps)));
    }

    public void setCurrentGravity(double newGravity) {
        universe.setGravity(newGravity);
//        Log.d("GRAVITY", Double.toString(newGravity));

    }
    public int getCurrentSteps() {
        return universe.getCurrentSteps();
    }
    public double getCurrentGravity() {
        return universe.getCurrentGravity();
    }

    public void draw() {

        if (holder.getSurface().isValid()) {
            canvas = holder.lockCanvas();

            canvas.save();

            super.draw(canvas);
            canvas.drawColor(1);

            canvas.translate(dx, dy);
            canvas.translate(Constants.UNIVERSEWIDTH / 2, Constants.UNVIVERSEHEIGTH / 2);
            canvas.scale(scaleFactor, scaleFactor);
            canvas.translate(-Constants.UNIVERSEWIDTH / 2 / scaleFactor, -Constants.UNVIVERSEHEIGTH / 2 / scaleFactor);


            if (canvas != null) {

                // draw boundary area
                canvas.drawRect(boundaryRect, boundaryPaint);
                // draw all objects
                universe.draw(canvas, isTraceMode);
            }
            canvas.restore();
            holder.unlockCanvasAndPost(canvas);
        }
    }


    private class ScaleListener
            extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            focusX = (detector.getFocusX()-dx)/scaleFactor;
            focusY = (detector.getFocusY()-dy)/scaleFactor;
//            Log.d("FOCUS", Float.toString(focusX) + " " + Float.toString(focusY));
            return true;
        }
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            if (isZoomMode) {
                scaleFactor *= detector.getScaleFactor();
                scaleFactor = Math.max(SCALEConstants.MINSCALE, Math.min(scaleFactor, SCALEConstants.MAXSCALE));
                invalidate();

            }
            return true;

        }
    }

    @Override
    public void run() {
        while (isRunning) {
            // Capture the current time in milliseconds in startFrameTime
            long startFrameTime = System.currentTimeMillis();

            // Update the frame
            this.update();

            // Draw the frame
            this.draw();
        }
    }

    public void resume(){
        isRunning = true;
        thread = new Thread(this);
        thread.start();
    }

    public void pause() {
        isRunning = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            Log.e("ERROR", "JOINING THREAD");
        }
    }
}
