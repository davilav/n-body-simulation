package com.example.projectastronomy.ui;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import com.example.projectastronomy.R;
import com.example.projectastronomy.utils.Constants;

public class MainActivity extends Activity {
    public GameView.RESET_TYPE loadTypeState;
    public GameView.ADD_TYPE addTypeState;
    public GameView.PLACEMENT_TYPE placementState;
    public GameView.SIZE_TYPE sizeState;
    public float scaleState;
    GameView gameView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        gameView = findViewById(R.id.gameView);

        Button resetButton = findViewById(R.id.resetButton);
        resetButton.setOnClickListener(v -> {
            gameView.reset(gameView.currentLoadType,scaleState);
        });

        loadTypeState = GameView.RESET_TYPE.BLANK;
        addTypeState = GameView.ADD_TYPE.PLANET;
        placementState = GameView.PLACEMENT_TYPE.SCATTER;
        sizeState = GameView.SIZE_TYPE.MEDIUM;


        Button addTypeButton = findViewById(R.id.addType);
        addTypeButton.setOnClickListener(v -> {
            showAlertDialogAddType();
        });

        Button loadTypeButton = findViewById(R.id.loadButton);
        loadTypeButton.setOnClickListener(v -> {
            showAlertDialogLoadType();
        });

        Button zoomButton = findViewById(R.id.zoomMode);
        zoomButton.setOnClickListener(v -> {
            if (!gameView.isZoomMode){
                    zoomButton.setAlpha(0.45f);
                    Toast.makeText(MainActivity.this, "Use drag move, pinch to zoom", Toast.LENGTH_LONG).show();
                }
                else{
                    zoomButton.setAlpha(1.f);
                    Toast.makeText(MainActivity.this, "Move mode disabled", Toast.LENGTH_LONG).show();

                }
                gameView.toggleZoomMode();
            });

        final Button traceButton = findViewById(R.id.traceMode);
        traceButton.setOnClickListener(v -> {
            if (!gameView.isTraceMode)
                {
                    traceButton.setAlpha(0.45f);
                    Toast.makeText(MainActivity.this, "Trace paths ON", Toast.LENGTH_LONG).show();
                }
                else
                {
                    traceButton.setAlpha(1.0f);
                    Toast.makeText(MainActivity.this, "Trace paths OFF", Toast.LENGTH_LONG).show();
                }
                gameView.toggleTraceMode();
        });

        final ImageButton pauseButton = findViewById(R.id.pauseplay);
         pauseButton.setOnClickListener(v -> {
            gameView.setCurrentDeltaT(Constants.dT_0);
        });

        // Speed button #1 (slow)
        final ImageButton speedButton1 = findViewById(R.id.speed1);
        speedButton1.setOnClickListener(v -> {
            gameView.setCurrentDeltaT(Constants.dT_1);

        });

        // Speed button #2 (medium)
        final ImageButton speedButton2 = findViewById(R.id.speed2);
        speedButton2.setOnClickListener(v -> {
            gameView.setCurrentDeltaT(Constants.dT_2);

        });
        // Speed button #3 (fast)
        final ImageButton speedButton3 = findViewById(R.id.speed3);
        speedButton3.setOnClickListener(v -> {
            gameView.setCurrentDeltaT(Constants.dT_3);

        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();

    }
    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();

    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void showAlertDialogLoadType() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle("Load preset");
        String[] list = GameView.RESET_TYPE.getString();
        final Button loadTypeButton = findViewById(R.id.loadButton);
        scaleState = GameView.SCALEConstants.DEFAULTSCALE;
        alertDialog.setSingleChoiceItems(list, loadTypeState.ordinal(),  (dialog, which) -> {
            switch(which){
                case 0:
                    Toast.makeText(MainActivity.this, "Clear all", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                    loadTypeState = GameView.RESET_TYPE.BLANK;
                    break;
                case 1:
                    Toast.makeText(MainActivity.this, "Single star system", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                    loadTypeState = GameView.RESET_TYPE.SINGLE_STAR_SYSTEM;
                    break;
                case 2:
                    Toast.makeText(MainActivity.this, "Binary star system", Toast.LENGTH_LONG).show();
                    loadTypeState = GameView.RESET_TYPE.BINARY_STAR_SYSTEM;
                    break;
                case 3:
                    Toast.makeText(MainActivity.this, "Black hole accretion disk", Toast.LENGTH_LONG).show();
                    loadTypeState = GameView.RESET_TYPE.BLACK_HOLE_ACCELERATION;
                    break;
            }
            dialog.dismiss();
            gameView.setCurrentResetType(loadTypeState);
            gameView.reset(loadTypeState, scaleState);
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }

    private void showAlertDialogAddType() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle("Celestial Body");
        String[] list = GameView.ADD_TYPE.getString();
        final Button addTypeButton = (Button)findViewById(R.id.addType);

        alertDialog.setSingleChoiceItems(list, addTypeState.ordinal(), (dialog, which) -> {
                switch (which) {
                    case 0:
                        Toast.makeText(MainActivity.this, "Asteroid", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                        addTypeState = GameView.ADD_TYPE.ASTEROID;
                        break;
                    case 1:
                        Toast.makeText(MainActivity.this, "Planet", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                        addTypeState = GameView.ADD_TYPE.PLANET;
                        break;
                    case 2:
                        Toast.makeText(MainActivity.this, "Star", Toast.LENGTH_LONG).show();
                        addTypeState = GameView.ADD_TYPE.STAR;
                        break;
                    case 3:
                        Toast.makeText(MainActivity.this, "Black Hole", Toast.LENGTH_LONG).show();
                        addTypeState = GameView.ADD_TYPE.BLACK_HOLE;
                        break;
                    case 4:
                        Toast.makeText(MainActivity.this, "Satellite", Toast.LENGTH_LONG).show();
                        addTypeState = GameView.ADD_TYPE.SATELITE;
                        break;
                    case 5:
                        Toast.makeText(MainActivity.this, "Player ship: Drag to control", Toast.LENGTH_LONG).show();
                        addTypeState = GameView.ADD_TYPE.PLAYER_SHIP;
                        gameView.isplayerShipMode = true;
                        break;
                }
                dialog.dismiss();
                addTypeButton.setText(GameView.ADD_TYPE.getString()[addTypeState.ordinal()]);
                gameView.setCurrentAddType(addTypeState);
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }

}








