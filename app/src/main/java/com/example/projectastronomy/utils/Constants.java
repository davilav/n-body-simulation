package com.example.projectastronomy.utils;

import com.example.projectastronomy.astro_objects.Universe;

public class Constants {
    //Creamos el ancho y el largo del universo con respecto a la pantalla
    public static final int UNIVERSEWIDTH = Universe.getScreenWidth()*5;
    public static final int UNVIVERSEHEIGTH= Universe.getScreenHeight()*5;
    //
    public static final double G = 2000000000.0;
    //Maximum force allowed to be exerted. This solves problem of extreme
    // acceleration when two objects are near each other
    public static final double MAXFORCE = 1000000000;
    // The exponent to raise distance when calculating gravitational force,
    // In real life, this is 2 (Gmm/r^2)
    //But modify it to adjust how "heavy" gravity feel
    public static final double EPSILON = 2;
    //Integración,

//   Time step for integration
//   More steps = more precise
    public static final double STEPS_0 = 0;
    public static final double dT_0 = 0;
    public static final double STEPS_1 = 50000;
    public static final double dT_1 = 1.0/ STEPS_1;
    public static final double STEPS_2 = 10000;
    public static final double dT_2 = 1.0/ STEPS_2;
    public static final double STEPS_3 = 5000;
    public static final double dT_3 = 1.0/ STEPS_3;
}
