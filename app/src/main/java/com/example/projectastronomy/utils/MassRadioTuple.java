package com.example.projectastronomy.utils;

public class MassRadioTuple {
    public double mass;
    public double radio;
    //Metodo constructor
    public MassRadioTuple(double new_mass,double new_radio){
        mass = new_mass;
        radio = new_radio;
    }
}