package com.example.projectastronomy.utils;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.projectastronomy.ui.GameView;

import java.util.Random;

public class ColorGenerator {
    static StarColor starColor;
    //Metodo constructor para crear un objeto de StarColor
    public ColorGenerator(){
        starColor = new StarColor();
    }
    //Metodo para pintar de acuerdo al cuerpo celeste
    public static Paint generateColor(GameView.ADD_TYPE celestialBodyType,GameView.SIZE_TYPE size_type){
        Random rand = new Random();
        Paint paint = new Paint();
        //Evaluamos los casos para saber que cuerpo celeste vamos a pintar
        switch (celestialBodyType) {
            case ASTEROID:
                int gray = rand.nextInt(100) + 100;
                paint.setColor(Color.rgb(gray, gray, gray));
                break;
            case PLANET:
                Color planetColor = Color.valueOf(Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));
                Color atmosphere = Color.valueOf(Color.rgb(255-planetColor.red(),255-planetColor.green(),255-planetColor.blue()));
                paint.setColor(planetColor.toArgb());
                paint.setShadowLayer(50,0,0,atmosphere.toArgb());
                break;
            case STAR:
                StarColor.StarColorInfo starCol = starColor.bvRgb(rand.nextDouble()*2.4f-0.4f);
                paint.setColor(Color.rgb(starCol.r,starCol.g,starCol.b));
                paint.setShadowLayer(50,0,0,Color.rgb(starCol.r,starCol.g,starCol.b));
                break;
            case BLACK_HOLE:
                paint.setColor(Color.rgb(0,0,0));
                paint.setShadowLayer(15,0,0, Color.WHITE);
                break;
            case SATELITE:
                int grayD = rand.nextInt(100) +100;
                paint.setColor(Color.rgb(grayD,grayD,grayD));
                paint.setShadowLayer(10,0,0,Color.GRAY);
                break;
            case PLAYER_SHIP:
                paint.setColor(Color.rgb(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255)));
                paint.setShadowLayer(50,0,0,Color.WHITE);
                break;

        }
        return paint;

    }
    // Generar un color pastel
    public static Paint generatePastel(Paint oldPaint,int passes){
        Paint pastel = new Paint();
        return pastel;
    }


}
