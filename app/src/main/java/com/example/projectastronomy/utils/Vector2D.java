package com.example.projectastronomy.utils;

public class Vector2D {
    //Creamos las variables x y y que representan las dimensiones del vector
    private double x;
    private double y;
    //Creamos el metodo constructor de la clase
    public Vector2D(double new_x,double new_y){
        x = new_x;
        y = new_y;
    }
    //Metodo constructor
    public void set(double new_x,double new_y){
        x = new_x;
        y = new_y;
    }
    //Creamos los getters and setters de las variables x y y para utilizarla en el metodo angle

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    //Creamos un metodo que retorna el angulo del vector

    public double angle(Vector2D vector2D){
        return Math.atan2(vector2D.getY()-y,vector2D.getX()-x);
    }
    //Retorna la distancia del vector
    public double distance(Vector2D vector2D){
        return Math.sqrt(Math.pow(x-vector2D.getX(),2) + Math.pow(y-vector2D.getY(),2));
    }
    //Producto punto de los dos vectores
    public double dotProduct(Vector2D vector2D){
        return (x*vector2D.getX()) + (y*vector2D.getY());

    }
    //Magnitud del vector
    public double magnitude(){
        return Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
    }

}
