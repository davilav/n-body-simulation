package com.example.projectastronomy.astro_objects;

import android.graphics.Color;
import android.graphics.Paint;

import com.example.projectastronomy.ui.GameView;
import com.example.projectastronomy.utils.MassRadioTuple;
import com.example.projectastronomy.utils.Vector2D;

// Se crea una clase que heredo de Celestial Body para crear objetos en la simulación (PLANETAS)
public class Planet extends CelestialBody {
    public static final class SIZE {
        //Se definen 3 tamaños para el planeta
        public static final MassRadioTuple SMALL = new MassRadioTuple(0.1, 5);
        public static final MassRadioTuple MEDIUM = new MassRadioTuple(1, 10);
        public static final MassRadioTuple BIG = new MassRadioTuple(10, 20);
    }

    // Se definen sus colores
    public Planet(GameView.SIZE_TYPE size, Paint planetPaint) {
        //Se hereda Paint para colorear el planeta
        planetPaint.setColor(Color.argb(255, rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)));
        // Se crearon 4 vectores para posición, velocidad, aceleración y fuerza
        Vector2D planetPos = new Vector2D(0, 0);
        Vector2D planetVel = new Vector2D(0, 0);
        Vector2D planetAcc = new Vector2D(0, 0);
        Vector2D planetForce = new Vector2D(0, 0);
        switch (size) {
            case SMALL:
                super.setMass(SIZE.SMALL.mass);
                super.setRadio(SIZE.SMALL.radio);
                break;
            case MEDIUM:
                super.setMass(SIZE.MEDIUM.mass);
                super.setRadio(SIZE.MEDIUM.radio);
                break;
            case LARGE:
                super.setMass(SIZE.BIG.mass);
                super.setMass(SIZE.BIG.radio);
                break;
            case RANDOM:
                double randRadio = SIZE.SMALL.radio + rand.nextDouble() * (SIZE.BIG.radio - SIZE.SMALL.radio);
                super.setRadio(randRadio);
                super.setMass(randRadio * SIZE.BIG.mass / SIZE.BIG.radio);
                break;
                

        }
        super.setPos(planetPos);
        super.setVel(planetVel);
        super.setAcc(planetAcc);
        super.setForce(planetForce);
        super.setPaint(planetPaint);
    }
}