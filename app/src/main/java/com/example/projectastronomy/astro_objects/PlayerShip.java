package com.example.projectastronomy.astro_objects;

import android.graphics.Color;
import android.graphics.Paint;

import com.example.projectastronomy.ui.GameView;
import com.example.projectastronomy.utils.MassRadioTuple;
import com.example.projectastronomy.utils.Vector2D;

public class PlayerShip extends CelestialBody {
    public static final class SIZE {
        //Se definen 3 tamaños para la nave
        public static final MassRadioTuple SMALL = new MassRadioTuple(0.000000001, 0.000001);
        public static final MassRadioTuple MEDIUM = new MassRadioTuple(0.00000001, 0.00001);
        public static final MassRadioTuple BIG = new MassRadioTuple(0.000001, 0.0001);
    }

    // Se definen sus colores
    public PlayerShip(GameView.SIZE_TYPE size, Paint playerShipPaint) {
        //Se hereda Paint para colorear la nave

        // Se crearon 4 vectores para posición, velocidad, aceleración y fuerza
        Vector2D playerShipPos = new Vector2D(0, 0);
        Vector2D playerShipVel = new Vector2D(0, 0);
        Vector2D playerShipAcc = new Vector2D(0, 0);
        Vector2D playerShipForce = new Vector2D(0, 0);
        switch (size) {
            case SMALL:
                super.setMass(PlayerShip.SIZE.SMALL.mass);
                super.setRadio(PlayerShip.SIZE.SMALL.radio);
                break;
            case MEDIUM:
                super.setMass(PlayerShip.SIZE.MEDIUM.mass);
                super.setRadio(PlayerShip.SIZE.MEDIUM.radio);
                break;
            case LARGE:
                super.setMass(PlayerShip.SIZE.BIG.mass);
                super.setMass(PlayerShip.SIZE.BIG.radio);
                break;
            case RANDOM:
                double randRadio = PlayerShip.SIZE.SMALL.radio + rand.nextDouble() * (PlayerShip.SIZE.BIG.radio - PlayerShip.SIZE.SMALL.radio);
                super.setRadio(randRadio);
                super.setMass(randRadio * PlayerShip.SIZE.BIG.mass / PlayerShip.SIZE.BIG.radio);
                break;


        }
        super.setPos(playerShipPos);
        super.setVel(playerShipVel);
        super.setAcc(playerShipAcc);
        super.setForce(playerShipForce);
        super.setPaint(playerShipPaint);
    }

    @Override
    public void control(){
        // Calcular el angulo del vector de la fuerza
        double theta_v = Math.atan2(super.getVel().getY(),super.getVel().getX());
        double theta_f = Math.atan2(super.getForce().getY(),super.getForce().getX());
        double velAbs = super.getVel().magnitude();
        Vector2D f = super.getForce();
        Vector2D a = super.getAcc();
        Vector2D v = super.getVel();
        //Setear la fuerza y la aceleración
        super.setForce(new Vector2D(-f.getX(),-f.getY()));
        super.setAcc(new Vector2D(-a.getX(),-a.getY()));
        //Establecer la velocidad a 90 grados del vector de la fuerza
        super.setVel(new Vector2D(velAbs*Math.cos(theta_f+Math.PI/2),velAbs*Math.sin(theta_f+Math.PI/2)));
        
    }
}
