package com.example.projectastronomy.astro_objects;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.view.MotionEvent;

import androidx.annotation.RequiresApi;

import com.example.projectastronomy.ui.GameView;
import com.example.projectastronomy.utils.ColorGenerator;
import com.example.projectastronomy.utils.Constants;
import com.example.projectastronomy.utils.Vector2D;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.example.projectastronomy.ui.GameView.ADD_TYPE.ASTEROID;
import static com.example.projectastronomy.ui.GameView.ADD_TYPE.BLACK_HOLE;
import static com.example.projectastronomy.ui.GameView.ADD_TYPE.PLANET;
import static com.example.projectastronomy.ui.GameView.ADD_TYPE.PLAYER_SHIP;
import static com.example.projectastronomy.ui.GameView.ADD_TYPE.SATELITE;
import static com.example.projectastronomy.ui.GameView.ADD_TYPE.STAR;

public class Universe {
    private List<Star> stars;
    private List<Planet> planets;
    private List<CelestialBody> objects;
    private List<CelestialBody> objectsAdd;
    private List<CelestialBody> objectsRemove;
    private ColorGenerator colorGenerator;
    private double currentGravity;
    private double currentDt;
    private double currentSteps;
    private Vector2D currentPlayers = new Vector2D(0,0);
    //Saber si la nave esta seleccionada
    public boolean isPlayer = false;
    private Vector2D currentPlayerForce = new Vector2D(0,0);
    public static Random rand = new Random();
    public Universe(GameView.RESET_TYPE resetType){
        //
        currentDt = Constants.dT_1;
        currentGravity = Constants.G;
        //Inicializamos los planetas,estrellas... objetos celestes
        stars = new ArrayList<>();
        planets = new ArrayList<>();
        objects = new ArrayList<>();
        objectsAdd = new ArrayList<>();
        objectsRemove = new ArrayList<>();
        colorGenerator = new ColorGenerator();
        //definimos los numeros de planetas
        int numPlanets = 100;
        switch (resetType){
            case BLANK:

                break;
            case SINGLE_STAR_SYSTEM:
                int numSinglePlanets = rand.nextInt(20)+1;
                int numSingleAsteroids =  rand.nextInt(100)+100;
                Star star = new Star(GameView.SIZE_TYPE.LARGE, ColorGenerator.generateColor(GameView.ADD_TYPE.STAR, GameView.SIZE_TYPE.LARGE));
                star.isFixed = true;
                star.setPos(new Vector2D(Constants.UNIVERSEWIDTH/2,Constants.UNVIVERSEHEIGTH/2));
                objects.add(star);
                Vector2D posSingleObjects;

                for (int as = 0; as < numSingleAsteroids; as++) {
                    //Esta definiendo una posicion respecto a la posicion de la estrella y su radio con una distancia random
                    posSingleObjects = randomDistanceFromStar(star.getPos(), star.getRadio(), Math.random()*star.getRadio()*10);
                    //Agregando un cuerpo celeste con la posicion
                    addCelestialBody(new Vector2D(posSingleObjects.getX(), posSingleObjects.getY()), new Vector2D(0,0),
                            0, GameView.ADD_TYPE.ASTEROID, GameView.SIZE_TYPE.RANDOM, GameView.PLACEMENT_TYPE.ORBIT);
                }

                break;


            case BINARY_STAR_SYSTEM:




                break;
            case BLACK_HOLE_ACCELERATION:



                break;

        }

    }

    public void update(){
        //Calcular la fuerza neta para cada objeto
        for(CelestialBody object1: objects){
            //Para cada objeto en celestial body
            Vector2D fNet = new Vector2D(0,0);
            Vector2D acc = new Vector2D(0,0);
            Vector2D vel = new Vector2D(0,0);
            Vector2D pos = new Vector2D(0,0);
            //Buscar un objeto que ejerza la mayor fuerza posible a object1
            CelestialBody maxForceObject = object1;
            double forceMax = 0;
            for(CelestialBody object2: objects){
                if (object1 == object2){
                    continue;
                }
                // Si el objeto1 tiene un valor conocido asignamos la fuerza neta como 0
                if (object1.isFixed){
                    fNet.setX(0);
                    fNet.setY(0);
                    object1.setVel(new Vector2D(0,0));
                    continue;
                }
                //Los asteroides ejercen una fuerza insignificante
                if (object2 instanceof Asteroid){
                    continue;
                }
                //Comprobamos si el objeto colisiona y no se fusiono
                if (object1.isCollide(object2) && !objectsRemove.contains(object1)){
                    handleCollide(object1,object2);
                    continue;
                }
                //Obtener atracción gravitacional
                Vector2D grav = object1.calculateGrav(object2, currentGravity);
                // Si el objeto actual esta en orbita, entonces se realiza un seguimiento del objeto más grande
                if(object1.isOrbit && grav.magnitude() > forceMax){
                    maxForceObject = object2;
                    forceMax = grav.magnitude();
                }
                //Incrementar la fuerza neta actual con la neta
                fNet.setX(fNet.getX() + grav.getX());
                fNet.setY(fNet.getY() + grav.getY());
            }
            // Si el objeto es una nave espacial, añadir fuerza al jugador
            if(object1 instanceof PlayerShip){
                fNet.setX(fNet.getX() + currentPlayers.getX());
                fNet.setY(fNet.getY() + currentPlayers.getY());
            }
            // Integrate by time step
            //Fnet = ma -> a = Fnet / m
            acc.setX(fNet.getX()/object1.getMass());
            acc.setY(fNet.getY()/object1.getMass());

            // Obtener la velocidad integrando la aceleracion
            vel.setX(object1.getVel().getX() + (acc.getX()*currentDt));
            vel.setY(object1.getVel().getY() + (acc.getY()*currentDt));
            //Obtener la posición integrando la velocidad
            pos.setX(object1.getPos().getX() + (vel.getX()*currentDt));
            pos.setY(object1.getPos().getY()+ (vel.getY()*currentDt));

            //Actualizar el objeto
            object1.setForce(fNet);
            object1.setAcc(acc);
            object1.setVel(vel);

            // Si el objeto está en órbita(Velocidad orbital), establecer la velocidad inicial en un angulo perpendicular al angulo del vector de la fuerza
            if(object1.isOrbit){
                //Se saco el angulo entre el origen
                double theta_f = Math.atan2(object1.getForce().getY(), object1.getForce().getX());
                //sacar velocidad orbital
                double vAbs = Math.sqrt(currentGravity*maxForceObject.getMass() / Math.pow(object1.getPos().distance(maxForceObject.getPos()),1));
                object1.setForce(new Vector2D(0,0));
                object1.setAcc(new Vector2D(0,0));
                object1.setVel(new Vector2D(vAbs*Math.cos(theta_f + Math.PI/2), vAbs*Math.sin(theta_f + Math.PI/2)));

                object1.isOrbit = false;
            }
            // Comprobar que los objetos que creamos esten en el limite de la pantalla; Si esta fuera pues lo elimina y si esta dentro lo crea
            if (pos.getX() > Constants.UNIVERSEWIDTH + Constants.UNIVERSEWIDTH / 4 || pos.getX() < -Constants.UNIVERSEWIDTH/4 || pos.getY() > Constants.UNVIVERSEHEIGTH + Constants.UNVIVERSEHEIGTH/4 || pos.getY() < -Constants.UNVIVERSEHEIGTH/4){
                objectsRemove.add(object1);
            }
            else{
                object1.setPos(pos);
            }
            // Objetos especiales como la nave y el satelite
            if(object1 instanceof Satelite){
                object1.control();
            }
        }

        // Hacer una cola de planetas para borrarlos
        for(CelestialBody object: objectsRemove){
            if(isPlayer && object instanceof PlayerShip){
                setPlayer(false);

            }
            objects.remove(object);
        }
        objectsRemove.clear();
        // añadir los planetas que el usuario crea
        for(CelestialBody object: objectsAdd){
            objects.add(object);
        }
        objectsAdd.clear();
    }
    //Dibujar los cuerpos
    public void draw(Canvas canvas, Boolean isTraceMode){
        for(CelestialBody object : objects){
            object.draw(canvas, isTraceMode);
        }
    }
    // Add planet on touch
    public void addPlanet(Vector2D pos, GameView.PLACEMENT_TYPE placementType) {
        Planet tempPlanet = new Planet(GameView.SIZE_TYPE.MEDIUM, colorGenerator.generateColor(PLANET,GameView.SIZE_TYPE.MEDIUM));
        tempPlanet.setPos(pos);
        if (objects.size() < 1000){
            switch (placementType){
                case SCATTER:
                    tempPlanet.setVel(new Vector2D(rand.nextInt(100000) * (rand.nextInt(2) == 1? -1 : 1),
                            rand.nextInt(100000) * (rand.nextInt(2) == 1? -1 : 1)));
                case TARGET:

            }
            objectsAdd.add(tempPlanet);
        }

    }

    //add star on touch

    public void addStar(Vector2D pos){
        if(objects.size() < 1000){
            Star tempStar = new Star(GameView.SIZE_TYPE.MEDIUM, colorGenerator.generateColor(STAR, GameView.SIZE_TYPE.MEDIUM));
            tempStar.setPos(pos);
            objectsAdd.add(tempStar);
        }
    }


    //Se crea un cuerpo celeste cuando se hace touch

    public void addCelestialBody(Vector2D pos, Vector2D vel, int action, GameView.ADD_TYPE addType, GameView.SIZE_TYPE sizeType, GameView.PLACEMENT_TYPE placementType) {
        CelestialBody tempObject = new CelestialBody();
        Paint tempPaint = colorGenerator.generateColor(addType, sizeType);

        switch(addType){
            case ASTEROID:
                tempObject = new Asteroid(sizeType, tempPaint);
                break;
            case PLANET:
                tempObject = new Planet(sizeType, tempPaint);
                break;
            case STAR:
                tempObject = new Star(sizeType, tempPaint);
                break;
            case BLACK_HOLE:
                tempObject = new BlackHole(sizeType, tempPaint);
                break;
            case SATELITE:
                tempObject = new Satelite(sizeType, tempPaint);
                break;
            case PLAYER_SHIP:
                if(!isPlayer){
                    tempObject = new PlayerShip(sizeType, tempPaint);
                    setPlayer(true);
                }else{
                    return;
                }
                break;
        }

        if(objects.size() < 1000){
            switch (placementType) {
                case SCATTER:
                    tempObject.setVel(new Vector2D(rand.nextInt(50000) * (rand.nextInt(2) == 1? -1 : 1),
                            rand.nextInt(50000) * (rand.nextInt(2) == 1? -1 : 1)));
                    tempObject.setPos(pos);
                    objectsAdd.add(tempObject);

                    break;
                case FLICK:
                    tempObject.setVel(new Vector2D(vel.getX()*20, vel.getY()*20));
                    tempObject.setPos(pos);
                    objectsAdd.add(tempObject);
                    break;

                case TARGET:
                    if (action == MotionEvent.ACTION_UP){
                        tempObject.setVel(new Vector2D(vel.getX()*300, vel.getY()*300));
                        tempObject.setPos(pos);
                        objectsAdd.add(tempObject);
                    }
                    break;
                case IDLE:
                    tempObject.setVel(new Vector2D(0,0));
                    tempObject.setPos(pos);
                    objectsAdd.add(tempObject);
                    break;
                case FIXED:
                    tempObject.setVel(new Vector2D(0,0));
                    tempObject.setPos(pos);
                    tempObject.isFixed = true;
                    objectsAdd.add(tempObject);
                    break;
                case ORBIT:
                    tempObject.setVel(new Vector2D(0,0));
                    tempObject.setPos(pos);
                    tempObject.isOrbit = true;
                    objectsAdd.add(tempObject);
                    break;

            }
        }
    }
    public void handleCollide(CelestialBody object1,CelestialBody object2){
        double velAbs1 = object1.getVel().magnitude();
        double velAbs2 = object2.getVel().magnitude();
        double distX = object1.getPos().getX() - object2.getPos().getX();
        double distY = object1.getPos().getY() - object2.getPos().getY();
        double pi = Math.PI- Math.atan2(distY,distX);
        double tetha1 = Math.atan2(object1.getVel().getY(),object1.getVel().getY());
        double tetha2 = Math.atan2(object2.getVel().getY(),object2.getVel().getY());
        //Un planeta grande absorberia un planeta pequeño (choque inelastico)
        //Se da la nueva velocidad despues del momentun
        double vx = (object1.getMass()*object1.getVel().getX() + object2.getMass()*object2.getVel().getX())/(object1.getMass() + object2.getMass());
        double vy = (object1.getMass()*object1.getVel().getY() + object2.getMass()*object2.getVel().getY())/(object1.getMass() + object2.getMass());
        //Si alguno de los dos objetos es mas grande que el otro entonces el otro objeto sumara sus masas, su radio y su velocidad
        if (object2.getMass() >= object1.getMass()){
            object2.setMass(object1.getMass()+object2.getMass());
            object2.setRadio(object2.getRadio()+(object1.getRadio()/object2.getRadio()));
            object2.setVel(new Vector2D(vx,vy));
            //Eliminara el objeto mas pequeño ya que no es uno solo.
            objectsRemove.add(object1);

        }
        else{
            object1.setMass(object1.getMass()+object2.getMass());
            object1.setRadio(object1.getRadio()+(object2.getRadio()/object1.getRadio()));
            object1.setVel(new Vector2D(vx,vy));
            objectsRemove.add(object2);
        }
    }
    public static int getScreenWidth(){
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }
    public static int getScreenHeight(){
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public int getCurrentSteps() {
        return (int) (1/currentDt);
    }
    public double getCurrentGravity(){
        return currentGravity;
    }
    public void setCurrentDt(double dt){
        currentDt = dt;
    }

    public void setGravity(double newGravity){
        currentGravity = newGravity;
    }

    public void setPlayer(boolean mode) {
        isPlayer = mode;
    }
    public void setPlayerControls(Vector2D newForce){
        currentPlayerForce = newForce;
    }
    public static int randRange(int min, int max){
        return rand.nextInt((max-min)+1) + min;
    }
    public static Vector2D randomDistanceFromStar(Vector2D starPos, double starRadio, double dist){
        double randAngle = Math.random()*Math.PI*2;
        Vector2D randPos = new Vector2D((int)(starPos.getX()+(starRadio*2+dist)*Math.cos(randAngle)),(int)(starPos.getY()+(starRadio*2+dist)*Math.sin(randAngle)));
        return randPos;
    }

}
