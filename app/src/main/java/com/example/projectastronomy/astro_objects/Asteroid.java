package com.example.projectastronomy.astro_objects;

import android.graphics.Color;
import android.graphics.Paint;

import com.example.projectastronomy.ui.GameView;
import com.example.projectastronomy.utils.MassRadioTuple;
import com.example.projectastronomy.utils.Vector2D;

public class Asteroid extends CelestialBody {
    public static final class SIZE {
        //Se definen 3 tamaños para el asteroide
        public static final MassRadioTuple SMALL = new MassRadioTuple(0.000001,1);
        public static final MassRadioTuple MEDIUM = new MassRadioTuple(0.00001,3);
        public static final MassRadioTuple BIG = new MassRadioTuple(0.00001,5);
    }

    // Se definen sus colores
    public Asteroid(GameView.SIZE_TYPE size, Paint asteroidPaint) {
        //Se hereda Paint para colorear el asteroide
        // Se crearon 4 vectores para posición, velocidad, aceleración y fuerza
        Vector2D asteroidPos = new Vector2D(0, 0);
        Vector2D asteroidVel = new Vector2D(0, 0);
        Vector2D asteroidAcc = new Vector2D(0, 0);
        Vector2D asteroidForce = new Vector2D(0, 0);
        switch (size) {
            case SMALL:
                super.setMass(Asteroid.SIZE.SMALL.mass);
                super.setRadio(Asteroid.SIZE.SMALL.radio);
                break;
            case MEDIUM:
                super.setMass(Asteroid.SIZE.MEDIUM.mass);
                super.setRadio(Asteroid.SIZE.MEDIUM.radio);
                break;
            case LARGE:
                super.setMass(Asteroid.SIZE.BIG.mass);
                super.setMass(Asteroid.SIZE.BIG.radio);
                break;
            case RANDOM:
                double randRadio = Asteroid.SIZE.SMALL.radio + rand.nextDouble() * (Asteroid.SIZE.BIG.radio - Asteroid.SIZE.SMALL.radio);
                super.setRadio(randRadio);
                super.setMass(randRadio * Asteroid.SIZE.BIG.mass / Asteroid.SIZE.BIG.radio);
                break;


        }
        super.setPos(asteroidPos);
        super.setVel(asteroidVel);
        super.setAcc(asteroidAcc);
        super.setForce(asteroidForce);
        super.setPaint(asteroidPaint);
    }
}
