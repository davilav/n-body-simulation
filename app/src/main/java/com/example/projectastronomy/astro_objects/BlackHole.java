package com.example.projectastronomy.astro_objects;

import android.graphics.Color;
import android.graphics.Paint;

import com.example.projectastronomy.ui.GameView;
import com.example.projectastronomy.utils.MassRadioTuple;
import com.example.projectastronomy.utils.Vector2D;

public class BlackHole extends CelestialBody {
    public static final class SIZE {
        //Se definen 3 tamaños para el agujero negro
        public static final MassRadioTuple SMALL = new MassRadioTuple(10000, 25);
        public static final MassRadioTuple MEDIUM = new MassRadioTuple(100000, 30);
        public static final MassRadioTuple BIG = new MassRadioTuple(200000, 75);
    }

    // Se definen sus colores
    public BlackHole(GameView.SIZE_TYPE size, Paint blackHole) {
        //Se hereda Paint para colorear el agujero negro

        // Se crearon 4 vectores para posición, velocidad, aceleración y fuerza
        Vector2D blackHolePos = new Vector2D(0, 0);
        Vector2D blackHoleVel = new Vector2D(0, 0);
        Vector2D blackHoleAcc = new Vector2D(0, 0);
        Vector2D blackHoleForce = new Vector2D(0, 0);
        switch (size) {
            case SMALL:
                super.setMass(BlackHole.SIZE.SMALL.mass);
                super.setRadio(BlackHole.SIZE.SMALL.radio);
                break;
            case MEDIUM:
                super.setMass(BlackHole.SIZE.MEDIUM.mass);
                super.setRadio(BlackHole.SIZE.MEDIUM.radio);
                break;
            case LARGE:
                super.setMass(BlackHole.SIZE.BIG.mass);
                super.setMass(BlackHole.SIZE.BIG.radio);
                break;
            case RANDOM:
                double randRadius =SIZE.SMALL.radio + rand.nextDouble()*(SIZE.BIG.radio - SIZE.SMALL.radio);
                super.setRadio(SIZE.SMALL.radio + rand.nextDouble()*(SIZE.BIG.radio - SIZE.SMALL.radio));
                super.setMass(randRadius * SIZE.BIG.mass/SIZE.BIG.radio);
                break;
    
        }
        super.setPos(blackHolePos);
        super.setVel(blackHoleVel);
        super.setAcc(blackHoleAcc);
        super.setForce(blackHoleForce);
        super.setPaint(blackHole);
    }
}
