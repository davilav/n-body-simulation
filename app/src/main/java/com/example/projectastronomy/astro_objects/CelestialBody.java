package com.example.projectastronomy.astro_objects;

import android.graphics.Canvas;
import android.graphics.Canvas;
import android.graphics.Rect;;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.projectastronomy.utils.Constants;
import com.example.projectastronomy.utils.Constants;
import com.example.projectastronomy.utils.Vector2D;
import com.example.projectastronomy.utils.Vector2D;


import java.util.ArrayList;
import java.util.Random;

public class CelestialBody {
    //Se crea una masa que determine la fuerza de la atracción gravitacional
    private double mass;
    //Se crea el radio que determina el tamaño del cuerpo
    private double radio;
    // Se crea un objeto vector que simula la posición
    private Vector2D pos;
    // Se crea un objeto vector que simula la velocidad
    private Vector2D vel;
    // Se crea un objeto vector que simula la aceleración
    private Vector2D acc;
    //Se crea un objeto vector que simula la fuerza neta
    private Vector2D force;
    // Se crea un color para pintar los objetos
    private Paint paint;
    // Crear una lista dinamica de vectores
    private ArrayList<Vector2D> traceList = new ArrayList<>();
    //Si el objeto es estacionario, entonces no se mueve
    public boolean isFixed = false;
    //Si el objeto esta inicialmente entrando en órbita
    public boolean isOrbit = false;
    //Generamos posiciones aleatorias en el espacio
    Random rand = new Random();

    public void draw(Canvas canvas, Boolean isTraceMode) {
        // Draw trace paths
        Paint noGlow = new Paint(paint);
        noGlow.setShadowLayer(0,0,0,00);
        if (isTraceMode) {
            for (int i = 0; i < traceList.size()-1; i++) {
                canvas.drawLine((float)traceList.get(i).getX(), (float)traceList.get(i).getY(), (float)traceList.get(i+1).getX(), (float)traceList.get(i+1).getY(), noGlow);
//                canvas.drawCircle((int)trace.getX(), (int)trace.getY(), (int)2, noGlow);

            }
        }
        traceList.add(new Vector2D(pos.getX(), pos.getY()));
        if (traceList.size() > 50) {
            traceList.remove(0);
        }

        canvas.drawCircle((int)pos.getX(), (int)pos.getY(), (int)radio, paint);

        if (this instanceof Satelite || this instanceof PlayerShip) {
            canvas.drawRect(new Rect((int)pos.getX()-(int)getRadio(), (int)pos.getY()-(int)getRadio(), (int)pos.getX()+(int)getRadio(), (int)pos.getY()+(int)getRadio()), paint);
        }
    }
    //Se desarrolla el metodo para calcular la fuerza de gravedad de dos objetos
    public Vector2D calculateGrav(CelestialBody object2, double currentGravity){
        //Se calcula la distancia del objeto
        double d =pos.distance(object2.getPos());
        //Se calcula la fuerza
        double fGravAbs = (currentGravity*object2.getMass()*mass/(Math.pow(d, Constants.EPSILON)));
        //Creamos un objeto de Vector2D
        Vector2D fGrav;
        //Damos el eje x y y
        double angle = pos.angle(object2.getPos());
        //Definimos los casos si el objeto choco o no
        if (!isCollide(object2)){
            //Evalua el caso en el que el objeto no esta chocado(la fuerza sera la misma y se descompone)
            fGrav = new Vector2D(fGravAbs*Math.cos(angle),fGravAbs*Math.sin(angle));
        }
        else {
            //Evalua el caso en el que el objeto esta colisionado
            //Retorna una posicion cero
            fGrav = new Vector2D(0,0);


        }
        return fGrav;
    }
    public void control(){}
    //Definimos el metodo que arroja un booleano en caso de que este o no colisionado
    public boolean isCollide(CelestialBody object2) {
        double d = pos.distance(object2.getPos());
        //Si la distancia es menor al radio mas el radio del otro objeto entonces sera verdadero
        //Si la distancia es mayor entonces sera falso
        return d < (radio + object2.getRadio());
    }
    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public Vector2D getPos() {
        return pos;
    }

    public void setPos(Vector2D pos) {
        this.pos = pos;
    }

    public Vector2D getVel() {
        return vel;
    }

    public void setVel(Vector2D vel) {
        this.vel = vel;
    }

    public Vector2D getAcc() {
        return acc;
    }

    public void setAcc(Vector2D acc) {
        this.acc = acc;
    }

    public Vector2D getForce() {
        return force;
    }

    public void setForce(Vector2D force) {
        this.force = force;
    }

    public Paint getPaint() {
        return paint;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    public ArrayList<Vector2D> getTraceList() {
        return traceList;
    }

    public void setTraceList(ArrayList<Vector2D> traceList) {
        this.traceList = traceList;
    }

    public boolean isFixed() {
        return isFixed;
    }

    public void setFixed(boolean fixed) {
        isFixed = fixed;
    }

    public boolean isOrbit() {
        return isOrbit;
    }

    public void setOrbit(boolean orbit) {
        isOrbit = orbit;
    }
    
}
