package com.example.projectastronomy.astro_objects;

import android.graphics.Paint;

import com.example.projectastronomy.ui.GameView;
import com.example.projectastronomy.utils.MassRadioTuple;
import com.example.projectastronomy.utils.Vector2D;

public class Star extends CelestialBody{
    public static final class SIZE {
        //Se definen 3 tamaños para la estrella
        public static final MassRadioTuple SMALL = new MassRadioTuple(1000, 25);
        public static final MassRadioTuple MEDIUM = new MassRadioTuple(3000, 50);
        public static final MassRadioTuple BIG = new MassRadioTuple(10000, 100);
    }

    // Se definen sus colores
    public Star(GameView.SIZE_TYPE size, Paint starPaint) {
        //Se hereda Paint para colorear la estrella
        

        // Se crearon 4 vectores para posición, velocidad, aceleración y fuerza
        Vector2D starPos = new Vector2D(0, 0);
        Vector2D starVel = new Vector2D(0, 0);
        Vector2D starAcc = new Vector2D(0, 0);
        Vector2D starForce = new Vector2D(0, 0);
        switch (size) {
            case SMALL:
                super.setMass(Star.SIZE.SMALL.mass);
                super.setRadio(Star.SIZE.SMALL.radio);
                break;
            case MEDIUM:
                super.setMass(Star.SIZE.MEDIUM.mass);
                super.setRadio(Star.SIZE.MEDIUM.radio);
                break;
            case LARGE:
                super.setMass(Star.SIZE.BIG.mass);
                super.setMass(Star.SIZE.BIG.radio);
                break;
            case RANDOM:
                double randRadio = Star.SIZE.SMALL.radio + rand.nextDouble() * (Star.SIZE.BIG.radio - Star.SIZE.SMALL.radio);
                super.setRadio(randRadio);
                super.setMass(randRadio * Star.SIZE.BIG.mass / Star.SIZE.BIG.radio);
                break;
        }
        super.setVel(starVel);
        super.setAcc(starAcc);
        super.setForce(starForce);
        super.setPaint(starPaint);
    }
}
