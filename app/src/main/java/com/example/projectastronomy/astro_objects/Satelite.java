package com.example.projectastronomy.astro_objects;

import android.graphics.Color;
import android.graphics.Paint;

import com.example.projectastronomy.ui.GameView;
import com.example.projectastronomy.utils.MassRadioTuple;
import com.example.projectastronomy.utils.Vector2D;

public class Satelite extends CelestialBody {
    public static final class SIZE {
        //Se definen 3 tamaños para el satelite
        public static final MassRadioTuple SMALL = new MassRadioTuple(0.000001,2);
        public static final MassRadioTuple MEDIUM = new MassRadioTuple(0.00001,5);
        public static final MassRadioTuple BIG = new MassRadioTuple(0.00001,7);
    }

    // Se definen sus colores
    public Satelite(GameView.SIZE_TYPE size, Paint satelitePaint) {
        //Se hereda Paint para colorear el satelite

        // Se crearon 4 vectores para posición, velocidad, aceleración y fuerza
        Vector2D satelitePos = new Vector2D(0, 0);
        Vector2D sateliteVel = new Vector2D(0, 0);
        Vector2D sateliteAcc = new Vector2D(0, 0);
        Vector2D sateliteForce = new Vector2D(0, 0);
        switch (size) {
            case SMALL:
                super.setMass(Satelite.SIZE.SMALL.mass);
                super.setRadio(Satelite.SIZE.SMALL.radio);
                break;
            case MEDIUM:
                super.setMass(Satelite.SIZE.MEDIUM.mass);
                super.setRadio(Satelite.SIZE.MEDIUM.radio);
                break;
            case LARGE:
                super.setMass(Satelite.SIZE.BIG.mass);
                super.setMass(Satelite.SIZE.BIG.radio);
                break;
            case RANDOM:
                double randRadio = Satelite.SIZE.SMALL.radio + rand.nextDouble() * (Satelite.SIZE.BIG.radio - Satelite.SIZE.SMALL.radio);
                super.setRadio(randRadio);
                super.setMass(randRadio * Satelite.SIZE.BIG.mass / Satelite.SIZE.BIG.radio);
                break;


        }
        super.setPos(satelitePos);
        super.setVel(sateliteVel);
        super.setAcc(sateliteAcc);
        super.setForce(sateliteForce);
        super.setPaint(satelitePaint);
    }

        @Override
    public void control(){
        // Calcular el angulo del vector de la fuerza
        double theta_v = Math.atan2(super.getVel().getY(),super.getVel().getX());
        double theta_f = Math.atan2(super.getForce().getY(),super.getForce().getX());
        double velAbs = super.getVel().magnitude();
        Vector2D f = super.getForce();
        Vector2D a = super.getAcc();
        Vector2D v = super.getVel();
        //Setear la fuerza y la aceleración
        super.setForce(new Vector2D(-f.getX(),-f.getY()));
        super.setAcc(new Vector2D(-a.getX(),-a.getY()));
        //Establecer la velocidad a 90 grados del vector de la fuerza
        super.setVel(new Vector2D(velAbs*Math.cos(theta_f+Math.PI/2),velAbs*Math.sin(theta_f+Math.PI/2)));

    }
}
